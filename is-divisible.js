function isDivisible(n, x, y) {
    return n % x == 0 && n % y == 0
}

//tests
console.log(isDivisible(9,3,9))
console.log(isDivisible(3,3,4))
console.log(isDivisible(12,3,4))
console.log(isDivisible(8,3,4))
console.log(isDivisible(48,3,4))